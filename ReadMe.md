# Weather api server

## Introduction

This application was built as part of a code test challenge and is meant to represent a basic weather forecast api.

This application is split up into two parts. The api which is found in the weather_backend folder and the database which is found in the weather_db folder.
To run these applications separately instructions can be found in their individual folders.

## Requirements and assumptions

The following features were required:

1. Display the name of your city with its current temperature, weather condition,
   highest/lowest temperature, as well as the weather forecast for the upcoming 3 days.
2. User can search the city
3. Save it into its shortlist.
4. Switch between the shortlisted cities by swiping the screen to left or right.
5. Assume there is a third-party service which are synchronizing the weather information into your database via the API you provide.

Requirement 4 appeared to be for an front end application and was ignored for this application.

Requirement 3 out of a consideration of time and how I personally would have implemented this requirement in a real world application I would save the shortlist data on the local device bypassing the need for user accounts.

## Application shortfalls

Several shortcuts were made during the creation of this application due to time constraints and may result in a less robust and user friendly application. They are as follows:

- Updating the weather for the city is a put request which requires the full city data and three day weather report. I didn't implement a patch request purely due to time constraints and the current put endpoint meet all the requirements.

- The current implementation doesn't support multiple cities with the same name. Obviously this would fail in the real world however any usable solution would have taken too much time implement for this exercise.

- No production scripts. Ideally I would implement production scripts for deployments but I thought doing so would be outside the scope of the application.

- The env file setup for the docker images is a bit clunky would have liked to spend a bit more time getting it more elegant.

- Env files shouldn't be included in the files.

- No database security.

- I had intended on including initial test data for the database but time constraints stopped me.

- No end to end testing.

## Installation and use.

1. Make sure you have the docker command line tool installed.
2. Run the command `make up` this should get everything running for you.
3. To end the service run the command `make down`.
