up:
	  docker-compose --env-file ./weather_backend/src/data/vars/.env.docker up -d

down: 
		docker-compose --env-file ./weather_backend/src/data/vars/.env.docker down