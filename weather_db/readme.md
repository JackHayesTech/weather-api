## Introduction.

The database solution for the application.

## Installation.

Running `make up` should install and get the application running.
Running `make down` will stop the application.
