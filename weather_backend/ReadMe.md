# Weather Backend

## Installation and use.

1. Make sure you have latest node up and running.
2. Run the command `npm i` to install the needed packages
3. Start the service with `npm run local`

To test the application run `npm test`
