import { Application } from 'express';
import { routes } from './Routes';

jest.mock('./services/health/health.routes');
jest.mock('./services/city/city.routes');

import { healthRoutes, cityRoutes } from './services';

describe('Should call each route function mock.', () => {
  beforeEach(() => jest.clearAllMocks());

  it('Should call the health routes mock', () => {
    const mock = healthRoutes as jest.Mock;
    routes(null as unknown as Application);
    expect(mock).toHaveBeenCalledTimes(1);
  });

  it('Should call the city routes mock', () => {
    const mock = cityRoutes as jest.Mock;
    routes(null as unknown as Application);
    expect(mock).toHaveBeenCalledTimes(1);
  });
});
