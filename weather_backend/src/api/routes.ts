import { Application } from 'express';
import { healthRoutes, cityRoutes } from './services';

/**
 * Assigns the application routes.
 * @param app - The express application.
 */
export const routes = (app: Application): void => {
  // Routes for returning the weather data for cities.
  cityRoutes(app);
  // Routes for application heath checks.
  healthRoutes(app);
};
