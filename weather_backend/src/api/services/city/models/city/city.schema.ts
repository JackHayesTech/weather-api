import { Schema } from 'mongoose';

import { getCity, createCity, searchCities } from './static';
import { lean } from './instance';
import { CityDocument, CityDocumentLean } from './city.model';

const weatherSchema = new Schema({
  // The current temperature for the city.
  currentTemp: Number,
  // What the weather is like.
  conditions: String,
  // The highest temperature for the city that day.
  high: Number,
  // The lowest temperature for teh city that day.
  low: Number,
  // The date of the weather.
  date: String,
});

// Schema
export const CitySchema = new Schema(
  {
    // The name of the city.
    name: String,
    // The weather forecast for this city.
    weather: {
      type: [weatherSchema],
      default: [],
    },
  },
  { strict: true },
);

CitySchema.statics.createCity = createCity;
CitySchema.statics.getCity = getCity;
CitySchema.statics.searchCities = searchCities;

CitySchema.methods.lean = function (): CityDocumentLean {
  return lean(this as CityDocument);
};
