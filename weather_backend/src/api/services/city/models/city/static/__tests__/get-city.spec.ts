import { getCity } from '../get-city';
import { CityModel } from '..';

describe('Tests for the get city.', () => {
  it('Should return the expected data.', async () => {
    const city = 'city';
    jest.spyOn(CityModel, 'findOne').mockResolvedValue(city as any);

    const res = await getCity('');

    expect(res).toEqual(city);
  });

  it('Should throw an error when the user id is not found.', async () => {
    jest.spyOn(CityModel, 'findOne').mockResolvedValue(null);
    let res;
    try {
      await getCity('');
    } catch (error) {
      res = error;
    }

    expect(res).toBeTruthy();
  });
});
