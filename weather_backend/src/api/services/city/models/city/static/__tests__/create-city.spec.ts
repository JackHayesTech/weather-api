import mockingoose from 'mockingoose';
import { CityModel } from '..';

import { createCity } from '../create-city';

describe('Tests for the add city function', () => {
  const name = 'name';
  const weather: any[] = [];
  const city = {
    name,
    weather,
  } as unknown as any;

  it('Should create a new city.', async () => {
    mockingoose(CityModel).toReturn(city, 'findOneAndUpdate');
    const res = await createCity(name, city);

    expect(res.name).toEqual(name);
  });

  it('Should return an error when failed to create new city.', async () => {
    mockingoose(CityModel).toReturn(() => {
      throw new Error();
    }, 'findOneAndUpdate');
    let res;

    try {
      await createCity(name, city);
    } catch (error) {
      res = error;
    }

    expect(res).toBeTruthy();
  });
});
