import { model } from 'mongoose';
import { ServerError, ResError } from '@jh-tech/response-object';

import { MODEL, CityDocument } from '.';

const noCityError = (): ServerError => {
  console.log('new');
  const status = 404;
  const detail = 'No city was found with the provided name.';
  const resError: ResError = {
    status,
    // WB - Weather backend, GC - Get city, NCE - No city error
    code: `WB_GC_NCE`,
    title: 'Incorrect city name was provided.',
    detail,
  };

  const error = new ServerError(detail, status, resError);

  return error;
};

/**
 * Returns the city.
 * @param name
 */
export const getCity = async (name: string): Promise<CityDocument> => {
  const { CITY } = MODEL;

  console.log('test');

  // Finds the city from the city name.
  const city = await model<CityDocument>(CITY).findOne({
    name,
  });

  console.log('test2');

  // No city was found with the provided city name.
  if (!city) throw noCityError();

  return city;
};
