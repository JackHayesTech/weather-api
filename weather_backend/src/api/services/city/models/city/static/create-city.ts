import { model } from 'mongoose';
import { ServerError, ResError } from '@jh-tech/response-object';

import { CityDocument, MODEL, CityDocumentLean } from '.';

// The query options for creating the city
const queryOptions = {
  upsert: true,
  new: true,
  setDefaultsOnInsert: true,
  useFindAndModify: false,
};

const createCityError = (err: string): ServerError => {
  const status = 500;
  const resError: ResError = {
    status,
    code: 'WB-CC-CCE',
    title: 'Error creating city.',
    detail: 'An error occurred when trying to create the city.',
  };

  return new ServerError(err, status, resError);
};

/**
 * Creates the city
 * @param user - The name of the city
 */
export const createCity = async (
  name: string,
  data: CityDocumentLean,
): Promise<CityDocument> => {
  const { CITY } = MODEL;
  const filter = { name };

  let city: CityDocument;

  try {
    // Updates or creates the city
    city = (await model<CityDocument>(CITY).findOneAndUpdate(
      filter,
      data,
      queryOptions,
    )) as CityDocument;
  } catch (error) {
    throw createCityError(error);
  }

  return city;
};
