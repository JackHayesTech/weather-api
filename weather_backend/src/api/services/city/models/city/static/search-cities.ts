import { model } from 'mongoose';
import { ServerError, ResError } from '@jh-tech/response-object';

import { MODEL, CityDocument } from '.';

const noCitiesFound = (): ServerError => {
  const status = 404;
  const detail = 'No city was found with the provided search data.';
  const resError: ResError = {
    status,
    // WB - Weather backend, GC - Get city, NCE - No city found
    code: `WB_GC_NCF`,
    title: 'No cities found',
    detail,
  };

  const error = new ServerError(detail, status, resError);

  return error;
};

/**
 * Returns a list of city names based on the input.
 * @param name
 */
export const searchCities = async (name: string): Promise<string[]> => {
  const { CITY } = MODEL;
  // Finds the city from the city name.
  const cities = await model<CityDocument>(CITY)
    .find({
      name: { $regex: '.*' + name + '.*' },
    })
    .distinct('name');

  // No user was found with the provided user id.
  if (cities.length === 0) throw noCitiesFound();

  return cities;
};
