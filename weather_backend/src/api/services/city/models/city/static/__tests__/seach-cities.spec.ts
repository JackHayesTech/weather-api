import { searchCities } from '../search-cities';
import { CityModel } from '..';

describe('Tests for the search cities.', () => {
  it('Should return the expected data.', async () => {
    const city = ['city'];

    const distinct = jest.fn().mockReturnValue(city);
    const find = jest.fn().mockReturnValue({ distinct } as any);
    jest.spyOn(CityModel, 'find').mockImplementation(find as any);

    const res = await searchCities('');

    expect(res).toEqual(city);
  });

  it('Should throw an error when the user id is not found.', async () => {
    const distinct = jest.fn().mockReturnValue([]);
    const find = jest.fn().mockReturnValue({ distinct } as any);
    jest.spyOn(CityModel, 'find').mockImplementation(find as any);

    let res;
    try {
      await searchCities('');
    } catch (error) {
      res = error;
    }

    expect(res).toBeTruthy();
  });
});
