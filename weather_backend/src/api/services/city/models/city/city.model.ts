import { Model, model, Document } from 'mongoose';

import { CitySchema } from './city.schema';
import { MODEL } from '..';

export interface Weather {
  // The current temperature for the city.
  currentTemp: number;
  // What the weather is like.
  conditions: string;
  // The highest temperature for the city that day.
  high: number;
  // The lowest temperature for teh city that day.
  low: number;
  // The date of the weather.
  date: string;
}

export interface CityDocumentLean {
  // The city name
  name: string;
  // The weather reports for the city.
  weather: Weather[];
}

/**
 * The city document represents the data for a city and its weather forecast.
 */
export interface BaseCityDocument extends CityDocumentLean, Document {
  // Returns pure data object from the city data.
  lean(): BaseCityDocument;
}

// Document
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface CityDocument extends BaseCityDocument {}

// Model
export interface CityModel extends Model<BaseCityDocument> {
  // Static methods
  /**
   * Creates city base on name.
   * @param name - The name of the city.
   * @param data - The city data.
   */
  createCity(name: string, data: CityDocumentLean): Promise<CityDocument>;

  /**
   * Returns city based on name.
   * @param name - The name of the city
   */
  getCity(name: string): Promise<CityDocument>;

  /**
   * Returns a list of city names based on search string
   * @param name - Search based on city names
   */
  searchCities(name: string): Promise<string[]>;

  // Instance methods
  /**
   * Returns a lean version of the city data
   */
  lean(): CityDocument;
}

export const CityModel = model<CityDocument, CityModel>(MODEL.CITY, CitySchema);
