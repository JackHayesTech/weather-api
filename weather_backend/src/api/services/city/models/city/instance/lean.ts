import { CityDocument, CityDocumentLean } from '.';

/**
 * Returns the stripped down version of the city.
 */
export const lean = (city: CityDocument): CityDocumentLean => ({
  name: city.name,
  weather: city.weather,
});
