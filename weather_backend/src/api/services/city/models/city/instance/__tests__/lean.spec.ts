import { lean } from '../lean';

describe('Tests for the lean function.', () => {
  it('Should return the expected data.', () => {
    const name = 'name';
    const weather = ['weather'];
    const fullCity = { name, weather, id: 'id' };
    const res = lean(fullCity as any);
    expect(res).toEqual({ name, weather });
  });
});
