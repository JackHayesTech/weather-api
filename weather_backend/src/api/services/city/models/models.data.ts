/**
 * Contains all the types of models used.
 */
export class MODEL {
  /**
   * City model
   */
  static readonly CITY = 'City';
}
