export { MODEL } from './models.data';

export { CityDocument, CityModel, CityDocumentLean } from './city/city.model';
