/**
 * Data file containing constants for creating the city routes.
 */

/**
 * Parameters in the url
 */
class PARAMS {
  static readonly NAME = 'name';
}

/**
 * Contains the routes used by the city endpoint.
 */
export class CITY_ROUTES {
  /** The base endpoint for this route */
  static readonly ENDPOINT = 'city';

  private static readonly base = `/${CITY_ROUTES.ENDPOINT}`;

  /** Object containing the params used by this endpoint. */
  static readonly PARAMS = PARAMS;

  // The endpoint for the search functionality.
  static readonly SEARCH = CITY_ROUTES.base;
  /** The get weather endpoint route. */
  static readonly GET_CREATE = `${CITY_ROUTES.base}/:${PARAMS.NAME}`;
}
