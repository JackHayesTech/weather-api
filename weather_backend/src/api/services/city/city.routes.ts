import { Application, Request, Response } from 'express';

import { getCityRoute, createCityRoute, searchCityRoute } from './endpoints';
import { CITY_ROUTES } from './city.routes.data';

/**
 * The routes for the city endpoints.
 * @param app - The express application.
 */
export const cityRoutes = (app: Application): void => {
  const { GET_CREATE, SEARCH } = CITY_ROUTES;

  app
    .route(SEARCH)
    .get((req: Request, res: Response) => searchCityRoute(req, res));

  app
    .route(GET_CREATE)
    // Returns the forecast for a specific city.
    .get((req: Request, res: Response) => getCityRoute(req, res))
    // Creates/updates the forecast for a city.
    .put((req: Request, res: Response) => createCityRoute(req, res));
};
