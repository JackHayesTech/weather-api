import { Resource } from '@jh-tech/response-object';

import { CityModel, MODEL } from '..';

/**
 * Returns the requested weather from the db.
 * @param name - The name of the city weather data we are requesting.
 */
export const getCity = async (name: string): Promise<Resource> => {
  const { CITY } = MODEL;
  // Returns the city from the db based on the query passed in.
  const city = await CityModel.getCity(name);
  const attributes = city.lean();
  const id = city.id;
  // Converts the city to the resources structure.
  return {
    id,
    type: CITY,
    attributes,
  };
};
