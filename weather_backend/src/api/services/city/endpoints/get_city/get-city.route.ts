import { Request, Response } from 'express';
import { ResponseObject } from '@jh-tech/response-object';

import { getCity } from './get-city.controller';

/**
 * Returns a city and its weather.
 * @param req - The express request object.
 * @param res - The express response object.
 */
export const getCityRoute = async (
  req: Request,
  res: Response,
): Promise<void> => {
  const {
    params: { name },
    params,
  } = req;
  // The response object used to format and return the data created during the request.
  const resObj = new ResponseObject(res, params);
  const { resData } = resObj;

  try {
    // sets the city object in the res data.
    resData.data = await getCity(name);
  } catch (error) {
    resData.addError(error);
  }

  // Returns the data to the user.
  resObj.sendResponse();
};
