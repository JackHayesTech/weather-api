import { getCity } from '../get-city.controller';

import { CityModel, MODEL } from '../..';

describe('Tests for the get city controller function', () => {
  const { CITY } = MODEL;
  it('Should return the expected data correctly', async () => {
    const id = 'id';
    const name = 'name';
    const weather: any[] = [];
    const newCity = { name, weather } as any;
    const createdCity = { id, lean: () => newCity } as any;
    const expected = {
      id,
      type: CITY,
      attributes: newCity,
    };

    jest.spyOn(CityModel, 'getCity').mockResolvedValue(createdCity as any);

    const res = await getCity(name);
    expect(res).toEqual(expected);
  });
});
