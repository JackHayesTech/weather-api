import { Request, Response } from 'express';
import { ResponseObject } from '@jh-tech/response-object';

import { searchCity } from './search-city.controller';

/**
 * Returns cities based on the param request.
 * @param req - The express request object.
 * @param res - The express response object.
 */
export const searchCityRoute = async (
  req: Request,
  res: Response,
): Promise<void> => {
  const {
    params,
    query: { name },
  } = req;
  // The response object used to format and return the data created during the request.
  const resObj = new ResponseObject(res, params);
  const { resData } = resObj;

  try {
    // sets the city object in the res data.
    resData.data = await searchCity(name as string);
  } catch (error) {
    resData.addError(error);
  }

  // Returns the data to the user.
  resObj.sendResponse();
};
