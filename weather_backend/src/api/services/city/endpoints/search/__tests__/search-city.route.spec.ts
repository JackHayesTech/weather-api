import { Request, Response } from 'express';
jest.mock('../search-city.controller');

const sendMock = jest.fn();
const errorMock = jest.fn();
jest.mock('@jh-tech/response-object', () => {
  return {
    ResponseObject: jest.fn().mockImplementation(() => {
      return {
        resData: { data: null, addError: errorMock } as unknown as any,
        sendResponse: sendMock,
      };
    }),
  };
});

import { searchCityRoute } from '../search-city.route';
import { searchCity } from '../search-city.controller';

describe('Tests for the search city route.', () => {
  const name = 'name';
  const body = 'body';

  beforeEach(() => jest.clearAllMocks());

  it('Should call the send response method after a valid search city call.', async () => {
    (searchCity as jest.Mock).mockImplementation(jest.fn(() => 'data'));

    const req = { query: { name }, body } as unknown as Request;

    await searchCityRoute(req, null as unknown as Response);

    expect(sendMock).toHaveBeenCalled();
    expect(errorMock).not.toHaveBeenCalled();
  });

  it('Should call the add error method after an invalid get city call.', async () => {
    (searchCity as jest.Mock).mockImplementation(
      jest.fn(() => {
        throw new Error();
      }),
    );

    const req = { query: { name }, body } as unknown as Request;

    await searchCityRoute(req, null as unknown as Response);

    expect(sendMock).not.toHaveBeenCalled();
    expect(errorMock).toHaveBeenCalled();
  });
});
