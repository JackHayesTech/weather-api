import { searchCity } from '../search-city.controller';

import { CityModel, MODEL } from '../..';

describe('Tests for the search city controller function.', () => {
  const { CITY } = MODEL;
  it('Should return the expected data.', async () => {
    const name = 'name';
    const cities = [name];
    const expected = {
      id: 'search',
      type: CITY,
      attributes: cities,
    };

    jest.spyOn(CityModel, 'searchCities').mockResolvedValue(cities as any);

    const res = await searchCity(name);
    expect(res).toEqual(expected);
  });
});
