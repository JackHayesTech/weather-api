import { Resource } from '@jh-tech/response-object';

import { CityModel, MODEL } from '..';

/**
 * Returns the requested cities from the db.
 * @param name - The name of the city weather data we are requesting.
 */
export const searchCity = async (name: string): Promise<Resource> => {
  const { CITY } = MODEL;
  // Returns the city from the db based on the query passed in.
  const city = await CityModel.searchCities(name);
  // Converts the city to the resources structure.
  return {
    id: 'search',
    type: CITY,
    attributes: city,
  };
};
