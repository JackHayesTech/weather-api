import { createCity } from '../create-city.controller';

import { CityModel, MODEL } from '../..';

describe('Tests for the create city controller function.', () => {
  const { CITY } = MODEL;
  it('Should return the expected data.', async () => {
    const id = 'id';
    const name = 'name';
    const weather: any[] = [];
    const newCity = { name, weather } as any;
    const createdCity = { id, lean: () => newCity } as any;
    const expected = {
      id,
      type: CITY,
      attributes: newCity,
    };

    jest.spyOn(CityModel, 'createCity').mockResolvedValue(createdCity as any);

    const res = await createCity(name, newCity);
    expect(res).toEqual(expected);
  });
});
