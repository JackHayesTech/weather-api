import { Request, Response } from 'express';
jest.mock('../create-city.controller');

const sendMock = jest.fn();
const errorMock = jest.fn();
jest.mock('@jh-tech/response-object', () => {
  return {
    ResponseObject: jest.fn().mockImplementation(() => {
      return {
        resData: { data: null, addError: errorMock } as unknown as any,
        sendResponse: sendMock,
      };
    }),
  };
});

import { createCityRoute } from '../create-city.route';
import { createCity } from '../create-city.controller';

describe('Tests for the create city route.', () => {
  const name = 'name';
  const body = 'body';

  beforeEach(() => jest.clearAllMocks());

  it('Should call the send response method after a valid create city call.', async () => {
    (createCity as jest.Mock).mockImplementation(jest.fn(() => 'data'));

    const req = { params: { name }, body } as unknown as Request;

    await createCityRoute(req, null as unknown as Response);

    expect(sendMock).toHaveBeenCalled();
    expect(errorMock).not.toHaveBeenCalled();
  });

  it('Should call the add error method after an invalid create city call.', async () => {
    (createCity as jest.Mock).mockImplementation(
      jest.fn(() => {
        throw new Error();
      }),
    );

    const req = { params: { name }, body } as unknown as Request;

    await createCityRoute(req, null as unknown as Response);

    expect(sendMock).not.toHaveBeenCalled();
    expect(errorMock).toHaveBeenCalled();
  });
});
