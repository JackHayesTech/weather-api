import { Resource } from '@jh-tech/response-object';

import { CityModel, MODEL, CityDocumentLean } from '..';
/**
 * Returns the requested weather from the db.
 * @param name - The name of the city weather data we are requesting.
 */
export const createCity = async (
  name: string,
  data: CityDocumentLean,
): Promise<Resource> => {
  const { CITY } = MODEL;
  const city = await CityModel.createCity(name, data);
  // Converts the city to the resources structure.
  const attributes = city.lean();
  const id = city.id;
  return {
    id,
    type: CITY,
    attributes,
  };
};
