import { Request, Response } from 'express';
import { ResponseObject } from '@jh-tech/response-object';

import { createCity } from './create-city.controller';
import { CityDocumentLean } from '..';

/**
 * Creates/Replaces a city and its weather.
 * @param req - The express request object.
 * @param res - The express response object.
 */
export const createCityRoute = async (
  req: Request,
  res: Response,
): Promise<void> => {
  const {
    params: { name },
    params,
    body,
  } = req;
  // The response object used to format and return the data created during the request.
  const resObj = new ResponseObject(res, params);
  const { resData } = resObj;

  try {
    // Creates and returns the new city object.
    resData.data = await createCity(name, body as CityDocumentLean);
    resData.status = 201;
  } catch (error) {
    resData.addError(error);
  }

  // Returns the data to the user.
  resObj.sendResponse();
};
