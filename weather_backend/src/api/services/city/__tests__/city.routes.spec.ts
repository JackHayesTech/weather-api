import express, { Application } from 'express';
import request from 'supertest';

jest.mock('../endpoints/create_city/create-city.route');
jest.mock('../endpoints/get_city/get-city.route');
jest.mock('../endpoints/search/search-city.route');

import { cityRoutes } from '../city.routes';
import { createCityRoute, getCityRoute, searchCityRoute } from '../endpoints';

describe('Tests for the cities routes file.', () => {
  // Setup the health routs for testing.
  const app: Application = express();
  cityRoutes(app);

  const mockFunc = (req: Request, res: any): void => {
    res.status(200).json();
  };

  const test1: any[] = ['get', getCityRoute, '/city/name'];
  const test2: any[] = ['put', createCityRoute, '/city/name'];
  const test3: any[] = ['get', searchCityRoute, '/city'];

  it.each([test1, test2, test3])(
    'Should register the %p cities route.',
    async (method, mock, endpoint) => {
      (mock as jest.Mock).mockImplementation(mockFunc);
      const res = await (request(app) as any)[method](endpoint);
      expect(res.status).toEqual(200);
    },
  );
});
