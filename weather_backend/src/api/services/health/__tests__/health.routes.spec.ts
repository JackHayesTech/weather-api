import express, { Application } from 'express';
import request from 'supertest';

import { healthRoutes } from '../health.routes';
import { HEALTH_ROUTES } from '../health.routes.data';

describe('Tests for the health routes file.', () => {
  const { GET } = HEALTH_ROUTES;
  // Setup the health routs for testing.
  const app: Application = express();
  healthRoutes(app);

  it('Should register the get health route.', async () => {
    const res = await (request(app) as any)['get'](GET);
    expect(res.status).toEqual(200);
  });
});
