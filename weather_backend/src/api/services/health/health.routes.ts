import { Application, Request, Response } from 'express';

import { HEALTH_ROUTES } from './health.routes.data';

/**
 * The routes for the health endpoints.
 * @param app - The express application.
 */
export const healthRoutes = (app: Application): void => {
  const { GET } = HEALTH_ROUTES;

  app
    .route(GET)
    .get((req: Request, res: Response) => res.send({ status: 'running' }));
};
