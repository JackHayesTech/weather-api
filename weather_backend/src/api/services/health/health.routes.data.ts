/**
 * Contains the routes used by the health endpoint.
 */
export class HEALTH_ROUTES {
  /** The base endpoint for this route */
  static readonly GET = '/health';
}
