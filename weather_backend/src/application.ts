import express from 'express';

import { middleware } from './middleware/middleware';
import { routes } from './api/routes';

// Creates the express application.
const app = express();

// Applies the middleware functions to the application.
middleware(app);

// Applies the api routes to the application.
routes(app);

export default app;
