jest.mock('mongoose');
import mongoose from 'mongoose';

import { dbConnect } from '../db-connect';

describe('Tests for the db connect function.', () => {
  const mockExit = jest.spyOn(process, 'exit').mockImplementation();

  beforeEach(() => jest.clearAllMocks());

  it('Should not call exit process mock.', async () => {
    jest.spyOn(mongoose, 'connect').mockResolvedValue(true as any);

    await dbConnect();
    expect(mockExit).toBeCalledTimes(0);
  });

  it('Should call exit process mock.', async () => {
    jest.spyOn(mongoose, 'connect').mockRejectedValue('');
    await dbConnect();
    expect(mockExit).toBeCalledTimes(1);
  });
});
