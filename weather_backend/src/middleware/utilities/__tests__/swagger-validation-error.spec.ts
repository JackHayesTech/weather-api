import { ResponseObject } from '@jh-tech/response-object';
jest.mock('@jh-tech/response-object');

import { swaggerValidationError } from '../swagger-validation-error';

describe('Tests for the swagger validation error function.', () => {
  const mockRD = {
    addError: jest.fn(),
  };
  const mockSR = jest.fn();
  const mock = ResponseObject as jest.Mock<ResponseObject>;

  mock.mockImplementation(
    () =>
      ({
        resData: mockRD,
        sendResponse: mockSR,
      } as any),
  );
  it('Should run without error.', () => {
    const err: any = {
      errors: [
        {
          message: 'message',
          trace: 'trace',
        },
      ],
    };
    swaggerValidationError(
      err,
      { params: 'params' } as any,
      null as any,
      null as any,
    );
  });
});
