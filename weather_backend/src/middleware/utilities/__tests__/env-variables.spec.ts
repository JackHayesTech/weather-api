import { envVariables } from '../env-variables';

jest.mock('dotenv');
import { config } from 'dotenv';

describe('Tests for the env variables.', () => {
  const mockExit = jest.spyOn(process, 'exit').mockImplementation();
  beforeEach(() => jest.clearAllMocks());
  test('Should not throw an error when no error.', () => {
    (config as jest.Mock).mockReturnValue({});
    envVariables();
    expect(mockExit).toHaveBeenCalledTimes(0);
  });

  test('Should call the exit process when given an error on failure to find file.', () => {
    (config as jest.Mock).mockReturnValue({ error: true });
    envVariables();
    expect(mockExit).toHaveBeenCalledTimes(1);
  });
});
