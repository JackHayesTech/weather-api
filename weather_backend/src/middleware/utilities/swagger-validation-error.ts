import { Request, Response, NextFunction } from 'express';
import { ValidationError } from 'express-openapi-validator/dist/framework/types';
import {
  ResponseObject,
  ServerError,
  ResError,
} from '@jh-tech/response-object';

/**
 * The error that occurs a request fails the open api validation.
 */
const validationError = (detail: string, path: string): ServerError => {
  const status = 400;
  const resError: ResError = {
    status,
    // WB - Weather backend, SV - Swagger validation, VE - Validation error
    code: 'WB-SV-VE',
    title: 'An error with the swagger validation has occurred.',
    detail,
  };

  const error = new ServerError(path, status, resError);

  return error;
};

/**
 * Middleware for sending a proper response error when validation fails.
 * @param err - The error the occurred.
 * @param req - The express request object.
 * @param res - The express response object.
 * @param next - The request next function object.
 */
export const swaggerValidationError = (
  err: ValidationError,
  req: Request,
  res: Response,
  next: NextFunction, // eslint-disable-line @typescript-eslint/no-unused-vars
): void => {
  const { params } = req;
  // The response object used to format and return the data created during the request.
  const resObj = new ResponseObject(res, params);
  const trace = new Error().stack as string;
  err.errors.forEach((e) => {
    // Make the string more human readable by replacing double quotes with in the string (this leads to backslashes everywhere)
    const message = JSON.stringify(e).replace(/['"]+/g, "'");
    resObj.resData.addError(validationError(message, trace));
  });
  resObj.sendResponse();
};
