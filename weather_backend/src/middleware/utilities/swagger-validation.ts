import * as OpenApiValidator from 'express-openapi-validator';

/**
 * Middleware for sending a proper response error when validation fails.
 * TODO support for multiple swagger files.
 */
export const swaggerValidation = OpenApiValidator.middleware({
  apiSpec: `${process.env.NODE_PATH as string}/src/data/swagger/Weather.yaml`,
  validateRequests: true,
  validateResponses: true,
});
