import mongoose from 'mongoose';

/**
 * Function for connecting to the mongo db.
 */
export const dbConnect = async (): Promise<void> => {
  // TODO add security.

  const connectionParams: mongoose.ConnectionOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  };
  // mongoose instance connection url connection
  const {
    env: { WEATHER_DB_PORT, WEATHER_DB_NAME, WEATHER_DB_URL },
  } = process;
  const connectionString: string =
    `mongodb://${WEATHER_DB_URL}:${WEATHER_DB_PORT}/${WEATHER_DB_NAME}` as string;
  mongoose.Promise = global.Promise;

  try {
    await mongoose.connect(connectionString, connectionParams);
  } catch (error) {
    // TODO return this as a res error instead of exiting program.
    // TODO add errors if db connection goes down during runtime.
    console.log(JSON.stringify(error, null, 2));
    console.log(`error - connecting to database at. ${connectionString}`);
    process.exit();
  }

  console.log('Connected to db');
};
