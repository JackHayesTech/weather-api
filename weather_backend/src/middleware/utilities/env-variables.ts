import { config } from 'dotenv';

/**
 * Loads the env variables from the specified env file.
 */
export const envVariables = (): void => {
  const path = `${process.cwd()}/src/data/vars/.env.${process.env.NODE_ENV}`;
  const configuration = config({
    path,
  });

  if (configuration.error) {
    // TODO return this as a res error instead of exiting program.
    console.log(`Failed to load config file from ${path}`);
    process.exit(1);
  }
};
