jest.mock('./utilities/env-variables.ts');
jest.mock('./utilities/db-connect');

import express from 'express';
import { middleware } from './middleware';

describe('Tests for the middleware function.', () => {
  it('Should run without issue', () => {
    const app = express();

    expect(() => middleware(app)).not.toThrow();
  });
});
