import express, { Application } from 'express';

import {
  swaggerValidation,
  swaggerValidationError,
  envVariables,
  dbConnect,
} from './utilities';

/**
 * Applies our custom middleware functions.
 * @param app - The express application we are applying the middleware functions to.
 */
export const middleware = (app: Application): void => {
  // Enables json request data.
  app.use(express.json());
  // Loads the env variables into the application.
  envVariables();

  // Applies the swagger validation to the application.
  app.use(swaggerValidation);

  // If the swagger validation throws an error apply custom error.
  app.use(swaggerValidationError);

  // Connects to the mongo db.
  (async (): Promise<void> => await dbConnect())();
};
