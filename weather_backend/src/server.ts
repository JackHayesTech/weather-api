import http from 'http';
import application from './application';

const port: number = parseInt(process.env.PORT as string);

http
  .createServer(application)
  .listen(port, () => console.log(`Http app is running on port ${port}`));
