import application from '../application';

jest.mock('../api/routes');
jest.mock('../middleware/middleware');

describe('Tests for the application file.', () => {
  it('Should return a working application.', () => {
    expect(application).toBeTruthy();
  });
});
